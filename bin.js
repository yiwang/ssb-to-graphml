#!/usr/bin/env node
const run = require('./index');

run().then(linesOfText => {
  for (let line of linesOfText) {
    process.stdout.write(line);
  }
});
